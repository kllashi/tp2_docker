# Completez le Dockerfile afin de faire fonctionner le serveur Rails
FROM ruby:2.7.2

# Completez ici...
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
COPY . /myapp
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install

EXPOSE 3000

# Conservez les lignes ci-dessous
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

# N'oubliez pas la commande pour démarrer le serveur
CMD bundle exec rails server
